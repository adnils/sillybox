# SillyBox

A silly box with the acrobatic abilities of a jumping shrimp.

**This is just a test,** but if you feel inclined you can **try it [here.](http://upr.im/sillybox/)**

-----

![boxgif!](http://i.minus.com/ipTQSNeO71jNX.gif)

## Movement:
- Up / Spacebar: Jump
- Left / Right Arrowkeys: Left / Right

## (current) Features:
- Variable Jump Height (hold down jump key)
- Wall jumping
- Red headband
- Blue eyes
