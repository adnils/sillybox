(function() {
  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
})();

var canvas = document.getElementById("game"),
    ctx = canvas.getContext("2d"),
    width = 450,
    height = 500,
    player = {
      x: width/2,
      y: height - 5,
      width: 20,
      height: 20,
      speed: 5,
      velX: 0,
      velY: 0,
      jumping: false
    },
    keys = [],
    friction = 0.88,
    gravity = 0.3
    jumpHeight = 1.5,
    _jumpLimit = 15;

canvas.width = width;
canvas.height = height;
var canJump = true;
var jumpLimit = 0;
var onGround = true,
    onWall = false,
    onLeftWall = false,
    onRightWall = false;

var idle_player = new Image();
idle_player.src = "idle_player.png";
var left_player = new Image();
left_player.src = "left_player.png";
var right_player = new Image();
right_player.src = "right_player.png";
var right_player_falling = new Image();
right_player_falling.src = "right_player_falling.png";
var left_player_falling = new Image();
left_player_falling.src = "left_player_falling.png";


document.body.addEventListener("keydown", function(e) {
  keys[e.keyCode] = true;
});

document.body.addEventListener("keyup", function(e) {
  keys[e.keyCode] = false;
});

function update() {
  if (keys[38] || keys[32] && canJump) {
    // up arrow or space
    if (onGround && onWall) {
      player.velY = -player.speed*jumpHeight;
      player.jumping = true;
      jumpLimit = 1;
    } else if (onGround) {
      player.velY = -player.speed*jumpHeight;
      player.jumping = true;
      jumpLimit = 1;
    } else if (onLeftWall && !onGround) {
      player.velX+=18;
      player.velY = -player.speed*jumpHeight+10;
      player.jumping = true;
      jumpLimit = 1;
    } else if (onRightWall && !onGround) {
      player.velX-=18;
      player.velY = -player.speed*jumpHeight+10;
      player.jumping = true;
      jumpLimit = 1;
    } else if (jumpLimit && jumpLimit <= _jumpLimit) {
      player.velY = -player.speed*jumpHeight
      player.jumping = true;
      jumpLimit++;
    }
  }

  if (keys[39]) {
      // right arrow
      if (player.velX < player.speed) {
          player.velX++;
      }
  }

  if (keys[37]) {
      // left arrow
      if (player.velX > -player.speed) {
          player.velX--;
      }
  }

  player.velX *= friction;
  player.velY += gravity;

  player.x += player.velX;
  player.y += player.velY;

  // Check if player is on the ground
  if (player.y >= height-player.height) {
    player.y = height - player.height; // Don't fall through the ground
    player.jumping = false;
    onGround = true;
    player.velY = 0;
    jumpLimit = 0;
  } else {
    onGround = false;
  }

  // Check if player is hitting the ceiling
  if (player.y < 0) {
    player.y = 0;
    player.velY = 0;
    player.jumping = false;
  }

  // Check if player is on right wall
  if (player.x >= width-player.width) {
    player.x = width-player.width;
    onRightWall = true;
  } else if (!(player.x >= width-player.width)) {
    onRightWall = false;
  }
  // Check if player is on left wall
  if (player.x <= 0) {
    player.x = 0;
    onLeftWall = true;
  } else if (!(player.x <= 0)) {
    onLeftWall = false;
  }

  // Check if player is on any wall
  if (onRightWall || onLeftWall) {
    onWall = true;
    player.jumping = false;
  } else {
    onWall = false;
  }

  // jumpLimit
  if (!keys[38] && !keys[32] && player.jumping) {
    jumpLimit = 0;
  } else if (!player.jumping) {
    jumpLimit = 0;
  }

  // Jump rules
  if (!player.jumping && jumpLimit <= _jumpLimit) {
    canJump = true;
  } else if (player.jumping && jumpLimit <= _jumpLimit) {
    canJump = true;
  } else if (player.jumping && jumpLimit >= _jumpLimit) {
    canJump = false;
    jumpLimit = 0;
  }

  ctx.clearRect(0,0,width,height);
  // ctx.fillStyle = "blue";
  // ctx.fillRect(player.x, player.y, player.width, player.height); // draw player
  if (player.velX < -1 && player.velY > 1) {
    ctx.drawImage(left_player_falling, player.x, player.y);
  } else if (player.velX < -1) {
    ctx.drawImage(left_player, player.x, player.y);
  } else if (player.velX > 1 && player.velY > 1) {
    ctx.drawImage(right_player_falling, player.x-5, player.y);
  } else if (player.velX > 1) {
    ctx.drawImage(right_player, player.x-5, player.y);
  } else {
    ctx.drawImage(idle_player, player.x, player.y);
  }
  requestAnimationFrame(update);
}

window.addEventListener("load", function(){
  update();
  // //  Debug spam
  // setInterval(function() {
  //   console.log(jumpLimit);
  //   console.log("canJump: " + canJump);
  //   console.log("onGround: " + onGround);
  //   console.log("onWall: " + onWall);
  //   console.log("velX: " + player.velX);
  //   console.log("velY: " + player.velY);
  // },300);
});